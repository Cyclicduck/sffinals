package sf.codingcomp.blocks;

public class BlockParser {
	public Block getNextBlock(String blockData) {
		String[] fields = blockData.split(",");
		if (fields[0].equals("claim")) {
			Claim cl = new Claim();
			cl.setAtFault(Integer.parseInt(fields[1]));
			cl.setRecipient(Integer.parseInt(fields[2]));
			cl.setAmountOfDamage(Double.parseDouble(fields[3]));
			cl.setDamageType(DamageType.valueOf(fields[4]));
			return new ClaimBlock(cl);
		} else if (fields[0].equals("Payment")) {
			Payment pa = new Payment();
			pa.setPayer(Integer.parseInt(fields[1]));
			pa.setPayee(Integer.parseInt(fields[2]));
			pa.setAmount(Double.parseDouble(fields[3]));
			return new PaymentBlock(pa);
		} else if (fields[0].equals("client")) {
			ClientRecord cr = new ClientRecord();
			cr.setClientID(Integer.parseInt(fields[1]));
			cr.setPassword(fields[2]);
			return new ClientRecordBlock(cr);
		}
		return null;
	}
}
