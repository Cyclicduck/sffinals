package sf.codingcomp.blocks;

public class Payment {

    private int payer;
    private int payee;
    private double amount;
    private int payerAge; 
    private int payerCreditScore;
    private int payerMonthsLate;
    private int duration;
    private int pastClaims;
    
    public double[] getAllData(){
    	double[] da = {payer, payee, amount, payerAge, payerCreditScore, payerMonthsLate, duration, pastClaims};
    	return da;
    }
    
    public int getPayerAge() {
        return payerAge;
    }
    public void setPayerAge(int payer) {
        this.payerAge = payer;
    }
    
    public int getPayerCreditScore() {
        return payerCreditScore;
    }

    public void setPayerCreditScore(int payer) {
        this.payerCreditScore = payer;
    }
    
    public int getPayerMonthsLate() {
        return payerMonthsLate;
    }

    public void setDuration(int payer) {
        this.duration = payer;
    }
    
    public int getDuration() {
        return duration;
    }

    public void setPastClaims(int payer) {
        this.pastClaims = payer;
    }
    
    public int getPastClaims() {
        return pastClaims;
    }

    public void setPayer(int payer) {
        this.payer = payer;
    }

    public int getPayee() {
        return payee;
    }

    public void setPayee(int payee) {
        this.payee = payee;
    }

    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(amount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + payee;
        result = prime * result + payer;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Payment other = (Payment) obj;
        if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
            return false;
        if (payee != other.payee)
            return false;
        if (payer != other.payer)
            return false;
        return true;
    }

}