package sf.codingcomp.blocks;
import java.io.*;
import java.util.*;

public class Demo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Blockchain chain = new Blockchain();
		BlockParser parser = new BlockParser();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("clientdata.csv")));
		Scanner sc = new Scanner(System.in);
		while (br.ready()) {
			try {
			chain.addBlock(parser.getNextBlock(br.readLine())); }
			catch (Exception e) {continue;}
		}
		System.out.println("Client and Claims History");
		while (true) {
			System.out.println("Enter command:");
			String input = sc.next();
			if (input.equals("client")) {
				System.out.println("Client login:");
				System.out.println("Enter client id:");
				int userID = sc.nextInt();
				System.out.println("Enter password:");
				String password = sc.next();
				if (!chain.verifyClientPassword(userID, password)) {
					System.out.println("Incorrect client ID or password.");
					continue;
				}
				while (true) {
					System.out.println("Which information would you like to query?");
					System.out.println("options: [claims, faults]");
					System.out.println("Type 'q' to quit.");
					String response = sc.next();
					if (response.equals("q")) {
						break;
					}
					if (response.equals("claims")) {
						ArrayList<Claim> claims = chain.getClientClaims(userID, password);
						System.out.printf("Claims with recipient %d:\n", userID);
						for (Claim cl: claims) {
							System.out.printf("Recipient: %d, At fault: %d, Amount of damage: %f\n", cl.getRecipient(), cl.getAtFault(), cl.getAmountOfDamage());							
						}
						System.out.printf("Total damage amount: %f\n", chain.getTotalClaims(userID, password));
					}
					if (response.equals("faults")) {
						ArrayList<Claim> faults = chain.getClientFaults(userID, password);
						System.out.printf("Claims with %d at fault:\n", userID);
						for (Claim cl: faults) {
							System.out.printf("Recipient: %d, At fault: %d, Amount of damage: %f\n", cl.getRecipient(), cl.getAtFault(), cl.getAmountOfDamage());							
						}
						System.out.printf("Total damage amount: %f\n", chain.getTotalCaused(userID, password));
					}
				}
			}
		}
	}

}
