package sf.codingcomp.blocks;

public class ClientRecord {
	private String password;
	private int id;
	public void setClientID(int id) {
		this.id = id;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getClientID() {
		return id;
	}
	public String getPassword() {
		return password;
	}
}
