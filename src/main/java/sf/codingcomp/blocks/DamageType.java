package sf.codingcomp.blocks;

public enum DamageType {
	Theft, Accident, Maintenance;
}
