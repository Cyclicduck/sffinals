package sf.codingcomp.blocks;

import java.util.*;

public class Blockchain<T> {

    private Collection<Block<T>> chain;
    private String firstHash;
    private Block<T> finalBlock;
    public Blockchain() {
        super();
        // TODO initialize chain to whatever collection type you want
        Block<T> genesisBlock = new Block<T>(null);
        genesisBlock.setHash(VerificationHashFactory.buildVerificationHash(genesisBlock));
        firstHash = genesisBlock.getHash();
        // TODO add the genesis block as the first block in the chain
        chain = new LinkedList<Block<T> >();
        chain.add(genesisBlock);
        finalBlock = genesisBlock;
    }
    
    

    public void addBlock(Block<T> block) {
    	block.setPreviousHash(finalBlock.getHash());
    	block.setHash(VerificationHashFactory.buildVerificationHash(block));
    	chain.add(block);
    	finalBlock = block;
    }

    public boolean verify() {
    	String prevHash = "";
    	for (Block<T> block: chain) {
    		if (!block.getHash().equals(VerificationHashFactory.buildVerificationHash(block))) {
    			return false;
    		}
    	}
    	for (Block<T> block: chain) {
    		if (!block.getHash().equals(firstHash)) {
    			if (block.getPreviousHash() == null || !(block.getPreviousHash().equals(prevHash))){
    				return false;
    			}
    		}
    		prevHash = block.getHash();
    	}
        return true;
    }
    
    public boolean verifyClientPassword(int client, String password) {
    	// get most recent password for client by examining blockchain
    	Class clientClass = new ClientRecordBlock(new ClientRecord()).getClass();
    	String updatedPassword = "";
    	for (Block<T> block: chain) {
    		if (block.getClass() != clientClass) {
    			continue;
    		}
    		if (((ClientRecord)block.getData()).getClientID() == client) {
    			updatedPassword = ((ClientRecord)block.getData()).getPassword();
    		}
    	}
    	return (updatedPassword.equals(password) && !(password.equals("")));
    }
    
    public int getTypeCount(DamageType type) {
    	Class claimClass = new ClaimBlock(new Claim()).getClass();
    	int typeCount = 0;
    	for (Block<T> block: chain) {
    		if (block.getClass() == claimClass && ((Claim)(block.getData())).getDamageType() == type) {
    			++typeCount;
    		}
    	}
    	return typeCount;
    }
    
    public double getTypeAverage(DamageType type) {
    	Class claimClass = new ClaimBlock(new Claim()).getClass();
    	double typeTotal = 0.0;
    	int typeCount = getTypeCount(type);
    	for (Block<T> block: chain) {
    		if (block.getClass() == claimClass && ((Claim)(block.getData())).getDamageType() == type) {
    			typeTotal += ((Claim)block.getData()).getAmountOfDamage();
    		}
    	}
    	return typeTotal/typeCount;
    }
    
    public double getTypeSD(DamageType type) {
    	Class claimClass = new ClaimBlock(new Claim()).getClass();
    	double typeTotal = 0.0;
    	int typeCount = getTypeCount(type);
    	double typeAvg = getTypeAverage(type);
    	for (Block<T> block: chain) {
    		if (block.getClass() == claimClass && ((Claim)(block.getData())).getDamageType() == type) {
    			typeTotal += Math.pow(((Claim)block.getData()).getAmountOfDamage()-typeAvg, 2);
    		}
    	}
    	return Math.sqrt(typeTotal/typeCount);
    }
    
    public ArrayList<Claim> getClientClaims(int client, String password)  {
    	if (!verifyClientPassword(client, password)) {
    		//throw new AccessDeniedException(String.format("Incorrect password for client #%d", client));
    		return new ArrayList<Claim>();
    	}
    	
    	ArrayList<Claim> claims = new ArrayList<Claim>();
    	Class claimClass = new ClaimBlock(new Claim()).getClass();
    	
    	for (Block<T> block: chain) {
    		if (block.getClass() == claimClass) {
    			if (((Claim)block.getData()).getRecipient() == client) {
    				claims.add((Claim)(block.getData()));
    			}
    		}
    	}
    	return claims;
    }
    
    public ArrayList<Claim> getClientFaults(int client, String password) {
    	ArrayList<Claim> claims = new ArrayList<Claim>();
    	Class claimClass = new ClaimBlock(new Claim()).getClass();
    	
    	for (Block<T> block: chain) {
    		if (block.getClass() == claimClass) {
    			if (((Claim)block.getData()).getAtFault() == client) {
    				claims.add((Claim)(block.getData()));
    			}
    		}
    	}
    	return claims;
    }
    
    /*
     * Returns the total value of claims by the client.
     */
    public double getTotalClaims(int client, String password) {
    	double total = 0.0;
    	for (Claim cl: getClientClaims(client, password)) {
    		total += cl.getAmountOfDamage();
    	}
    	return total;
    }
    
    /*
     * Returns the total value of claims in which the client was at fault.
     */
    public double getTotalCaused(int client, String password) {
    	double total = 0.0;
    	for (Claim cl: getClientFaults(client, password)) {
    		total += cl.getAmountOfDamage();
    	}
    	return total;
    }
}
