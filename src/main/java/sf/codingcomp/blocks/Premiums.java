package sf.codingcomp.blocks;

import java.util.*;

public class Premiums<Payment> {
	public Collection<Block<Payment>> chain;
	public HashMap<Block<Payment>, Integer> training;
	 
	
	public HashMap<Integer, Integer> getPremiums(){
		HashMap<Integer, Integer> premiums = new HashMap<Integer, Integer>();
		double[] coeff = getCoefficients();
		
		for (Block<Payment> block: chain){
			int ans = 0;
			double[] temp = ((Block<Payment>) block.getData()).getAllData();
			for(int i = 0; i < 6; i++){
				ans += temp[i+2] * coeff[i];
			}
			premiums.put((Integer.valueOf( (int) temp[0])),(Integer)ans);
		}
		
		return premiums;
	}
	
	public double[] getCoefficients(){
		/**
		 *  private int payer;
		    private int payee;
		    private double amount;
		    private int payerAge; 
		    private int payerCreditScore;
		    private int payerMonthsLate;
		    private int duration;
		    private int pastClaims;
		 */
		double[] coeff = new double[6];
		
		double[][] data = new double[training.size()][7];
		
		double[] a = {10, -5, -10, 10, 5, 10};
		return a;
		
		//Set<Block<Payment>> keys = training.keySet();
//		int ind = 0;
//		for(Block<Payment> key:keys){
//			Payment pa = (Payment) key.getData();
//			pa.
//			data[ind][0] = (pa).getAmount();
//			ind++;
//		}
		
		
//		
//		Set set = training.entrySet();
//		Iterator i = set.iterator();
//		int ind = 0;
//		while(i.hasNext()) {
//	         
//	         
//	         Block<Payment> bl = i.next().getKey();
//	         
//	         System.out.print(me.getKey() + ": ");
//	         System.out.println(me.getValue());
//	      }
//		
//		return coeff;
//		
		
		
	}
}
